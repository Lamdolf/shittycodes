#!/usr/bin/sh

set -e
set -u

# Takes a route and renames every file as a number with leading zeros.
# it can go from 1 to 999 hordcoded, but easily extensible (more hardcodding)
ROUTE=$1
counter=1
for FILE in $(/usr/bin/ls $ROUTE); do 
    fileFormat=$(basename $FILE | awk 'BEGIN{FS="."}{print $2}')
    # Rename to tmp in case file has the same from the beginning
    mv $ROUTE/$FILE $ROUTE/$FILE.tmp
    if [ $counter -lt 10 ]; then 
        mv $ROUTE/$FILE.tmp $ROUTE/000$counter.$fileFormat
    fi
    if [ $counter -lt 100 ] && [ $counter -ge 10 ]; then
        mv $ROUTE/$FILE.tmp $ROUTE/00$counter.$fileFormat
    fi
    if [ $counter -lt 1000 ] && [ $counter -ge 100 ]; then
        mv $ROUTE/$FILE.tmp $ROUTE/0$counter.$fileFormat
    fi
    counter=$((counter+1))
done

    
